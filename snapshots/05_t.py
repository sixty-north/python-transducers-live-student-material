"""A module t.py in which we'll do live coding."""

from functools import reduce
from itertools import chain
from transforms import is_prime, square
from functional import compose


def mapping(transform):

    def mapping_transducer(reducer):

        def map_reducer(result, item):
            return reducer(result, transform(item))

        return map_reducer

    return mapping_transducer


def filtering(predicate):

    def filtering_transducer(reducer):

        def filter_reducer(result, item):
            return reducer(result, item) if predicate(item) else result

        return filter_reducer

    return filtering_transducer


def appender(result, item):
    result.append(item)
    return result


def conjoiner(result, item):
    return type(result)(chain(result, (item,)))
