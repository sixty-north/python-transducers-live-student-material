"""A module t.py in which we'll do live coding."""

from functools import reduce
from transforms import is_prime, square


def mapping(transform):

    def mapping_transducer(reducer):

        def map_reducer(result, item):
            return reducer(result, transform(item))

        return map_reducer

    return mapping_transducer


def filtering(predicate):

    def filtering_transducer(reducer):

        def filter_reducer(result, item):
            return reducer(result, item) if predicate(item) else result

        return filter_reducer

    return filtering_transducer


def appender(result, item):
    result.append(item)
    return result
