"""A module t.py in which we'll do live coding."""

from functools import reduce
from itertools import chain
from transforms import is_prime, square
from functional import compose


class Mapping:

    def __init__(self, reducer, transform):
        self._reducer = reducer
        self._transform = transform

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        return self._reducer.step(result, self._transform(item))

    def complete(self, result):
        return self._reducer.complete(result)


def mapping(transform):

    def mapping_transducer(reducer):
        return Mapping(reducer, transform)

    return mapping_transducer


class Filtering:

    def __init__(self, reducer, predicate):
        self._reducer = reducer
        self._predicate = predicate

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        return self._reducer.step(result, item) if self._predicate(item) else result

    def complete(self, result):
        return self._reducer.complete(result)


def filtering(predicate):

    def filtering_transducer(reducer):
        return Filtering(reducer, predicate)

    return filtering_transducer


class Appending:

    def initial(self):
        return []

    def step(self, result, item):
        result.append(item)
        return result

    def complete(self, result):
        return result


class Conjoining:

    def initial(self):
        return tuple()

    def step(self, result, item):
        return type(result)(chain(result, (item,)))

    def complete(self, result):
        return result


UNSET = object()


def transduce(transducer, reducer, iterable, init=UNSET):
    r = transducer(reducer)
    accumulator = init if (init is not UNSET) else reducer.initial()
    for item in iterable:
        accumulator = r.step(accumulator, item)
    return r.complete(accumulator)

