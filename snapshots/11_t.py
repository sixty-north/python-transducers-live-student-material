"""A module t.py in which we'll do live coding."""

from functools import reduce
from itertools import chain
from transforms import is_prime, square
from functional import compose, true


class Mapping:

    def __init__(self, reducer, transform):
        self._reducer = reducer
        self._transform = transform

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        return self._reducer.step(result, self._transform(item))

    def complete(self, result):
        return self._reducer.complete(result)


def mapping(transform):

    def mapping_transducer(reducer):
        return Mapping(reducer, transform)

    return mapping_transducer


class Filtering:

    def __init__(self, reducer, predicate):
        self._reducer = reducer
        self._predicate = predicate

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        return self._reducer.step(result, item) if self._predicate(item) else result

    def complete(self, result):
        return self._reducer.complete(result)


def filtering(predicate):

    def filtering_transducer(reducer):
        return Filtering(reducer, predicate)

    return filtering_transducer


class Enumerating:

    def __init__(self, reducer, start):
        self._reducer = reducer
        self._counter = start

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        index = self._counter
        self._counter += 1
        return self._reducer.step(result, (index, item))

    def complete(self, result):
        return self._reducer.complete(result)


def enumerating(start=0):

    def enumerating_transducer(reducer):
        return Enumerating(reducer, start)

    return enumerating_transducer


class Batching:

    def __init__(self, reducer, size):
        self._reducer = reducer
        self._size = size
        self._pending = []

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        self._pending.append(item)
        if len(self._pending) == self._size:
            batch = self._pending
            self._pending = []
            return self._reducer.step(result, batch)
        return result

    def complete(self, result):
        r = self._reducer.step(result, self._pending) if len(self._pending) > 0 else result
        return self._reducer.complete(r)


def batching(size):

    if size < 1:
        raise ValueError("batching() size must be at least 1")

    def batching_transducer(reducer):
        return Batching(reducer, size)

    return batching_transducer


class First:

    def __init__(self, reducer, predicate):
        self._reducer = reducer
        self._predicate = predicate

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        return Reduced(self._reducer.step(result, item)) if self._predicate(item) else result

    def complete(self, result):
        return self._reducer.complete(result)


def first(predicate=None):
    predicate = true if predicate is None else predicate

    def first_transducer(reducer):
        return First(reducer, predicate)

    return first_transducer


class Repeating:

    def __init__(self, reducer, num_times):
        self._reducer = reducer
        self._num_times = num_times

    def initial(self):
        return self._reducer.initial()

    def step(self, result, item):
        for i in range(self._num_times):
            result = self._reducer.step(result, item)
        return result

    def complete(self, result):
        return self._reducer.complete(result)


def repeating(num_times):

    if num_times < 1:
        raise ValueError("num_times must be at least 1")

    def repeating_transducer(reducer):
        return Repeating(reducer, num_times)

    return repeating_transducer


class Appending:

    def initial(self):
        return []

    def step(self, result, item):
        result.append(item)
        return result

    def complete(self, result):
        return result


class Conjoining:

    def initial(self):
        return tuple()

    def step(self, result, item):
        return type(result)(chain(result, (item,)))

    def complete(self, result):
        return result


class ExpectingSingle:

    def __init__(self):
        self._num_steps = 0

    def initial(self):
        return None

    def step(self, result, item):
        assert result is None
        self._num_steps += 1
        if self._num_steps > 1:
            raise RuntimeError("Too many steps!")
        return item

    def complete(self, result):
        if self._num_steps < 1:
            raise RuntimeError("Too few steps!")
        return result


class Reduced:

    def __init__(self, value):
        self._value = value

    @property
    def value(self):
        return self._value

UNSET = object()


def transduce(transducer, reducer, iterable, init=UNSET):
    r = transducer(reducer)
    accumulator = init if (init is not UNSET) else reducer.initial()
    for item in iterable:
        accumulator = r.step(accumulator, item)
        if isinstance(accumulator, Reduced):
            accumulator = accumulator.value
            break
    return r.complete(accumulator)

